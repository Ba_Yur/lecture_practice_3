import 'package:flutter/material.dart';
import 'package:lecture_practice_3/main_layout.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lecture_practice_3/green_page.dart';

class DarkPage extends StatelessWidget {

  static final routeName = '/dark-page';

  final String name = 'You came from Dark page';

  final String text;

  const DarkPage({this.text});

  void changePage(BuildContext context) {
    Navigator.of(context).pushNamed(GreenPage.routeName, arguments: name);
  }

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('Dark page'),
      ),
      color: Colors.grey,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              text,
              style: TextStyle(fontSize: 40.h),
            ),
            RaisedButton(
              onPressed: () => changePage(context),
              child: Text('To Green Page'),
            ),
          ],
        ),
      ),
    );
  }
}
