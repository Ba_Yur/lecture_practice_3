import 'package:flutter/material.dart';
import 'package:lecture_practice_3/main_layout.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lecture_practice_3/green_page.dart';

class InitialPage extends StatelessWidget {

  static final routeName = '/initial-page';

  final String name = 'You came from initial Page';

  final String text;

  const InitialPage({this.text});

  void changePage(BuildContext context) {
    Navigator.of(context).pushNamed(GreenPage.routeName, arguments: name);
  }

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('Initial page'),
      ),
      color: Colors.blue,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              text,
              style: TextStyle(fontSize: 40.h),
            ),
            RaisedButton(
              onPressed: () => changePage(context),
              child: Text('Tap me'),
            ),
          ],
        ),
      ),
    );
  }
}
