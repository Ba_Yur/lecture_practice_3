import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lecture_practice_3/dark_page.dart';
import 'package:lecture_practice_3/green_page.dart';

import 'initial_page.dart';
import 'main_layout.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: Size(300, 600),
        builder: () => MaterialApp(
              theme: ThemeData(
                textTheme: TextTheme(
                  button: TextStyle(fontSize: 40.sp),
                ),
              ),
              title: 'Flutter Demo',
              // home: SomePage(),
              initialRoute: InitialPage.routeName,
              routes: {
                InitialPage.routeName: (ctx) => InitialPage(
                      text: 'Initial page',
                    ),
              },
              onGenerateRoute: (settings) {
                if (settings.name == GreenPage.routeName) {
                  final args = settings.arguments as String;
                  return MaterialPageRoute(builder: (ctx) {
                    return GreenPage(text: args);
                  });
                }
                if (settings.name == DarkPage.routeName) {
                  final args = settings.arguments as String;
                  return MaterialPageRoute(builder: (ctx) {
                    return DarkPage(text: args);
                  });
                }
                return MaterialPageRoute(
                  builder: (ctx) {
                    return InitialPage(text: 'It looks like some problems');
                  },
                );
              },
            ));
  }
}
