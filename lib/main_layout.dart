import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  final Widget child;
  final PreferredSizeWidget appBar;
  final Color color;

  MainLayout({
    @required this.child,
    @required this.appBar,
    @required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar,
      body: Container(
        margin: EdgeInsets.all(30),
        color: color,
        child: child,
      ),
    );
  }
}
