import 'package:flutter/material.dart';
import 'package:lecture_practice_3/dark_page.dart';
import 'package:lecture_practice_3/main_layout.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GreenPage extends StatelessWidget {

  static final routeName = '/some-page';

  final String name = 'You came from Green page';

  final String text;

  const GreenPage({this.text});

  void changePage(BuildContext context) {
    print(DarkPage.routeName);
    Navigator.of(context).pushNamed(DarkPage.routeName, arguments: name);
  }

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: AppBar(
        title: Text('GreenPage'),
      ),
      color: Colors.green,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              text,
              style: TextStyle(fontSize: 20.h),
            ),
            RaisedButton(
              onPressed: () => changePage(context),
              child: Text('To Dark Page'),
            ),
            SizedBox(
              height: 50,
            ),
            RaisedButton(
              onPressed: () => {
                Navigator.of(context).pushNamed('/')
              },
              child: Text('Path nowhere'),
            ),
          ],
        ),
      ),
    );
  }
}
